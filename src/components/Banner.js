//React boostrap components
import { Button, Carousel } from 'react-bootstrap';

import img1 from '../images/image1.jpg';
import img2 from '../images/image2.jpg'
import img3 from '../images/image3.jpg'


export default function Banner(props){
	return (

	<Carousel>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={img1}
		      alt="First slide"
		    />
		    <Carousel.Caption>
		      <h2>Demon Slayer</h2>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={img2}
		      alt="Second slide"
		    />

		    <Carousel.Caption>
		      <h2>One Piece</h2>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="d-block w-100"
		      src={img3}
		      alt="Third slide"
		    />

		    <Carousel.Caption>
		      <h2>My Hero Academia</h2>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>


	)
}