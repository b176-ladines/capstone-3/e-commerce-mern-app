import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import img1 from '../images/product.png';
export default function ProductCard({productProp}){

	console.log(productProp);

	console.log(typeof productProp);

	const {image, name, description, price, _id} = productProp

	return(
		<Card className="product-card col-12 col-lg-3 bg-danger mt-3 mx-1">
			
			<Card.Title>
				
				<h3 className="text-center text-white pt-5">{name}</h3>
				<img src={img1} className="rounded mx-auto d-block"/>
			</Card.Title>
			<Card.Body className="text-center">
				<h5>Description:</h5>
				<p>{description}</p>
				<h5>Price:</h5>
				<p>Php: {price}</p>

				<Button className="btn-white bg-white text-dark" as= {Link} to={`/products/${_id}`}>See Details</Button>
			</Card.Body>
		</Card>
	)
};
