import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import img1 from '../images/product.png'



export default function ProductView(){

	
	const [ count, setCount ] = useState(0);

	const {user} = useContext(UserContext);

	const history = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const {productId} = useParams();

	const buy = (productId) => {
		fetch('https://damp-thicket-45014.herokuapp.com/users/buy', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				name : name,
				description : description,
				price : price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){

				Swal.fire({
					title: "Added to Cart!",
					icon: "success",
					text: "You have successfully added to cart"
				})

				history("/products");

			} else {

				Swal.fire({
					title: "Something Went Wrong",
					icon: "error",
					text: "Please switch to user account!"
				})
			}
		})
	}



	useEffect(() => {
		console.log(productId)

		fetch(`https://damp-thicket-45014.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

		

return(
	<Container className="mt-5 bg-danger">
		<Row>
			<Col className="offset-4">
				<Card className="p-3">

					<Card.Body className="text-center">
						
						<Card.Title className="text-center">
						<h1 className="mb-3">{name}</h1></Card.Title>
						<img src={img1} className="rounded mx-auto d-block" />
						
						<Card.Subtitle className="pt-3">Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>Php {price}</Card.Text>

						<Card.Subtitle>Quantity</Card.Subtitle>
						<div id="mainDiv" className="m-3">
				            <button id="minus" onClick={()=>setCount(count-1)}>-</button>
				            <span id="numberPlace">{count}</span>
				            <button id="plus" onClick={()=>setCount(count+1)}>+</button>
				        </div>
						
						
						{ user.id !== null ?
							<Button className="btn-danger" onClick={() => buy(productId)}>Add to Cart</Button>
							:
							<Link className="btn btn-danger" to="/login">Log In to Buy</Link>
						}

					</Card.Body>
				</Card>
			</Col>
		</Row>
	</Container>

	)
}
