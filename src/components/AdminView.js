import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){
	const { productData, fetchData } = props;

	const [productId, setProductId] = useState("");
	const [products, setProducts] = useState([]);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	
	const openEdit = (productId) => {

		fetch(`https://damp-thicket-45014.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			setProductId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

		setShowEdit(true);
	};

	
	const closeEdit = () => {

		setShowEdit(false);
		setName("");
		setDescription("");
		setPrice(0);

	};

	const addProduct = (e) => {

		
		e.preventDefault()

		fetch(`https://damp-thicket-45014.herokuapp.com/products`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {
				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added."					
				})
				setName("")
				setDescription("")
				setPrice(0)

				closeAdd();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const deleteProduct = (productId) => {
			let token = localStorage.getItem('token');
			fetch(`https://damp-thicket-45014.herokuapp.com/products/${productId}/delete`, {
				method: "DELETE",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
			console.log(data)

			if (data !== true) {

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully deleted."
				});

				closeEdit();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		})
	}

	const editProduct = (e, productId) => {
		
		e.preventDefault();

		fetch(`https://damp-thicket-45014.herokuapp.com/products/${ productId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated."
				});

				closeEdit();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		})
	}

		const archiveToggle = (productId, isActive) => {

			console.log(!isActive);

			fetch(`https://damp-thicket-45014.herokuapp.com/products/${ productId }/archive`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {
					console.log(data)

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully archived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const unarchiveToggle = (productId, isActive) => {

			console.log(!isActive);

			fetch(`https://damp-thicket-45014.herokuapp.com/products/${ productId }/unarchive`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {
					console.log(data)

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully unarchived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		};

useEffect(() => {

		const productsArr = productData.map(product => {

			return(

				<tr key={product._id}>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>₱ {product.price}</td>
					<td>
						{product.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>
					<td className="text-center">
						<Button
							variant="primary"
							size="sm"
							onClick={() => openEdit(product._id)}
						>
							Update
						</Button>
						{product.isActive
							?
							<Button 
							    className="mx-2"
								variant="danger" 
								size="sm" 
								onClick={() => archiveToggle(product._id, product.isActive)}
							>
								Disable
							</Button>
							:
							<Button 
								className="mx-2"
								variant="success"
								size="sm"
								onClick={() => unarchiveToggle(product._id, product.isActive)}
							>
								Enable
							</Button>
						}
						<Button
							className="mx-2"
							variant="dark"
							size="sm"
							onClick={() => deleteProduct(product._id)}
						>
							Delete
						</Button>
					</td>
				</tr>

			)

		});

		setProducts(productsArr);

	}, [productData, fetchData]);

	return(
		<Fragment>

			<div className="text-center my-4">
				<h1 className="text-dark">Admin Dashboard</h1>
				<div className="d-flex justify-content-center">
					<Button className="btn-card bg-danger" onClick={openAdd}>Add New product</Button>			
				</div>			
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-white text-danger">
					<tr>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th className="text-center">Actions</th>
					</tr>					
				</thead>
				<tbody className="bg-Info">
					{products}
				</tbody>
			</Table>

			{/*ADD MODAL*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			{/*EDIT MODAL*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			
		</Fragment>
	)
}


