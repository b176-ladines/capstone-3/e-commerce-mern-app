import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Highlights() {
	return(
		<div >
		<h1 className="pt-3 text-center pb-2">Welcome To AnimeTaku!</h1>
		<h5 className="text-dark text-center">Are you an Anime Fan or an Otaku? This shop aims to provide you with a wide range of figures and merchandise for your favorite anime.</h5>
		<div className="page-content">
			<div className="cardP">
				<div className="content">
					<h2 className="title">Demon Slayer</h2>
					<Button className="btn-card bg-danger" as={ Link } to={'/products'}>Shop Now!</Button>
				</div>
			</div>
			<div className="cardP">
				<div className="content">
					<h2 className="title">One Piece</h2>
					<Button className="btn-card bg-danger" as={ Link } to={'/products'}>Shop Now!</Button>
				</div>
			</div>
			<div className="cardP">
				<div className="content">
					<h2 className="title">My Hero Academia</h2>
					<Button className="btn-card bg-danger" as={ Link } to={'/products'}>Shop Now!</Button>
				</div>
			</div>
		</div>
	</div>	
		)
}