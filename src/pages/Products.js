import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch('https://damp-thicket-45014.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, [])


	return(
		<>
		<h1 className="text-danger text-center p-1">Products</h1>
		<h4 className="text-dark text-center">Anime Figures</h4>
		<Container className="d-flex flex-wrap justify-content-center">
			{products}
		</Container>
		</>
	)
}

